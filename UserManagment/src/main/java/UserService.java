
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gaiga
 */
public class UserService {
    public static ArrayList<User> list = new ArrayList();

    public static ArrayList<User> getList() {
        return list;
    }
    
    public static void load(){
        System.out.println("Load");
        File f = null;
            FileInputStream fis = null;
            ObjectInputStream ois = null;
        try {
            
            f = new File("user.bin");
            fis = new FileInputStream(f);
            ois = new ObjectInputStream(fis);
            list = (ArrayList<User>)ois.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(User u:list){
            System.out.println(u);
        }
    }
    public static void save(ArrayList<User> list){
        System.out.println("Save");
        UserService.list = list;
        FileOutputStream fos = null;
        ObjectOutputStream oos =null;
        File f = null;
            
        try {
            f = new File("User.bin");
            fos = new FileOutputStream(f);
             oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public static boolean auth(String loginName,String password){
        for(User user : list){
            if(user.getLoginName().equals(loginName) && user.getPassword().equals(password))
                return true;
        }
        return false;
    }
}

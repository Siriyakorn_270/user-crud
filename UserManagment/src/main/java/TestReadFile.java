
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gaiga
 */
public class TestReadFile {
    public static void main(String[] args) {
        ArrayList<User> list = new ArrayList();
            File f = null;
            FileInputStream fis = null;
            ObjectInputStream ois = null;
        try {
            
            f = new File("user.bin");
            fis = new FileInputStream(f);
            ois = new ObjectInputStream(fis);
            list = (ArrayList<User>)ois.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(User u:list){
            System.out.println(u);
        }
    }
     
    
}
